#!/usr/bin/env bash

[ ! -z $NGINX_CONF ] && echo $NGINX_CONF | base64 -d - > /etc/nginx/nginx.conf
[ ! -z $LOG_CONF ] && echo $LOG_CONF | base64 -d - > /etc/nginx/log-default.json
[ ! -z $WAF_POLICY ] && echo $WAF_POLICY | base64 -d - > /etc/nginx/NginxDefaultPolicy.json

/bin/su -s /bin/bash -c '/opt/app_protect/bin/bd_agent &' nginx

/bin/su -s /bin/bash -c "/usr/share/ts/bin/bd-socket-plugin tmm_count 4 proc_cpuinfo_cpu_mhz 2000000 total_xml_memory 307200000 total_umu_max_size 3129344 sys_max_account_id 1024 no_static_config 2>&1 > /var/log/app_protect/bd-socket-plugin.log &" nginx

/usr/sbin/nginx -g 'daemon off;'
